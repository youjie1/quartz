package com.maple.core.task.service;

import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.maple.core.model.TaskModel;
import com.maple.core.quartz.SchedulerManager;
import com.maple.core.task.mapper.TaskMapper;

/**
 * TaskService
 *
 * @author leihz
 * @date 2017/7/5 17:17
 */
@Service
public class TaskService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TaskMapper taskMapper;
    @Autowired
    SchedulerManager schedulerManager;


    public List<TaskModel> getTaskList() {
        return taskMapper.getTaskList();
    }

    public List<TaskModel> getTaskListByParam(HashMap paramMap) {
        return taskMapper.getTaskListByParam(paramMap);
    }

    public List<TaskModel> getAllJobDetail(){return schedulerManager.getAllJobDetail();}
}
