package com.maple.core.task.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.maple.core.model.TaskModel;

/**
 * TaskMapper
 *
 * @author leihz
 * @date 2017/7/5 17:18
 */
@Mapper
public interface TaskMapper {

     List<TaskModel> getTaskList();

     List<TaskModel> getTaskListByParam(HashMap<String, Object> paramMap);
}
