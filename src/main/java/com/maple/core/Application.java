package com.maple.core;

import java.util.HashMap;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.maple.common.utils.SpringContextUtil;
import com.maple.core.model.TaskModel;
import com.maple.core.quartz.SchedulerManager;
import com.maple.core.task.service.TaskService;

/**
 * created by IntelliJ IDEA
 *
 * @author leihz
 * @date 2017/7/5 13:51
 */
@SpringBootApplication
public class Application {

	public static void main(String[] args) throws Exception {
		ApplicationContext app = SpringApplication.run(Application.class, args);
		/**将spring上下文set到SpringContextUtil中*/
		SpringContextUtil.setApplicationContext(app);

		SchedulerManager schedulerManager = (SchedulerManager)app.getBean("schedulerManager");
		TaskService taskService = (TaskService)app.getBean("taskService");
		HashMap<String,Object> paramMap = new HashMap<String, Object>();
		paramMap.put("task_status", "1");
		List<TaskModel> taskList = taskService.getTaskListByParam(paramMap);
		for (TaskModel task : taskList) {
			schedulerManager.createOrUpdateTask(task);
		}
	}
}
