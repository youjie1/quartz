package com.maple;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;



/**
 * 
 * 描述:一个简单的例子，反射与自定义注解
 *
 * @author maple
 * @date 2017年10月10日 下午2:52:30
 */
public class InvokeTest {
	public static void main(String... args) throws ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException {
		
		Class<?> model = Class.forName("xyz.youjieray.TaskModel");
		Class<?> model1= ModelTest123.class;
		Object obj = model.newInstance();
		
		Method[] methods = model.getMethods();
		for(Method m :methods){
			boolean present = m.isAnnotationPresent(Es.class);
			if(present){
				Es annotation = m.getAnnotation(Es.class);
				System.out.println(annotation.value());
				HashMap<String, Object> hashMap = new HashMap<String ,Object>();
				hashMap.put("123", "qqq");
				hashMap.put("456", "qqq123");
				
				m.invoke(obj, hashMap);
			}
		}

		//Class<?> caller = Reflection.getCallerClass();
		//Class.forName0("xyz.youjieray.TaskModel", true, ClassLoader.getClassLoader(caller), caller);
	         //   executeObj.getClass().getDeclaredMethod(tarteMethod,Object.class).invoke(executeObj,jobDataMap);

	}
}
